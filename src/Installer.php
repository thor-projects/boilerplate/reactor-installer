<?php

namespace Teras\ElementInstaller;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class Installer extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $prefix = substr($package->getPrettyName(), 0, 6);

        if ('teras/' !== $prefix) {
            throw new \InvalidArgumentException(
                'Unable to install element, teras elements should always start their package name with "teras/"'
            );
        }

        return substr($package->type, 6) . 's';
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return in_array($packageType, [
            'teras-module',
            'teras-component',
        ]);
    }
}